insert into book (id, title, authors, status) values (1, 'Wieza Asow', 'George R.R.Martin', 'FREE');
insert into book (id, title, authors, status) values (2, 'Gra o Tron', 'George R.R.Martin', 'FREE');
insert into book (id, title, authors, status) values (3, 'Szalejacy Dzokerzy', 'George R.R.Martin', 'FREE');
insert into book (id, title, authors, status) values (4, 'Java for Dummies', 'John Doe', 'FREE');
insert into book (id, title, authors, status) values (5, 'Java for not so dummy Dummies', 'John Doe', 'FREE');
insert into book (id, title, authors, status) values (6, 'Book One', 'Jane Doe', 'FREE');
insert into book (id, title, authors, status) values (7, 'Book Two', 'James Blunt', 'FREE');
insert into book (id, title, authors, status) values (8, 'Book Three', 'Peter Parker', 'FREE');
insert into book (id, title, authors, status) values (9, 'Book Four', 'James Jonah Jameson', 'FREE');
insert into book (id, title, authors, status) values (10, 'Book Five', 'John Doe', 'FREE');


insert into userentity (id, user_name, password) values (1, 'admin', 'admin');
insert into userentity (id, user_name, password) values (2, 'user', 'user');
insert into userentity (id, user_name, password) values (3, 'librarian', 'lib');