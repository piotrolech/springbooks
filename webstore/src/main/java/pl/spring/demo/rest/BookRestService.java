package pl.spring.demo.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.spring.demo.enumerations.BookStatus;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

@Controller
@ResponseBody
public class BookRestService {

	@Autowired
	BookService bookService;

	@RequestMapping(value = "/findAllBooks", method = RequestMethod.GET)
	public List<BookTo> findAllBooks() {
		return bookService.findAllBooks();
	}

	@RequestMapping(value = "/findtitle", method = RequestMethod.GET)
	public List<BookTo> findBooksByTitle(@RequestParam("title") String title) {
		return bookService.findBooksByTitle(title);
	}

	@RequestMapping(value = "/findBooks", method = RequestMethod.GET)
	public List<BookTo> findBooksByTitleAndAuthors(@RequestParam("title") String title,
			@RequestParam("authors") String authors) {
		return bookService.findBooksByTitleAndAuthor(title, authors);
	}

	@RequestMapping(value = "/findBooksArray", method = RequestMethod.GET)
	public List<BookTo> findBooksByTitleAndAuthors(@RequestParam(value = "params") String[] paramArray) {
		return bookService.findBooksByTitleAndAuthor(paramArray[0], paramArray[1]);
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public BookTo saveBook(@RequestBody BookTo bookTo) {
		return bookService.saveBook(bookTo);
	}

	@RequestMapping(value = "/book", method = RequestMethod.PUT)
	public BookTo addBook(@RequestBody BookTo bookTo) {
		return bookService.saveBook(bookTo);
	}

	@RequestMapping(value = "/book", method = RequestMethod.DELETE)
	public void deleteBook(@RequestBody BookTo bookTo) {
		bookService.deleteBook(bookTo.getId());
	}

}
