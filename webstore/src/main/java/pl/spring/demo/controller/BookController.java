package pl.spring.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.test.web.servlet.result.RequestResultMatchers;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.spring.demo.constants.ModelConstants;
import pl.spring.demo.constants.ViewNames;
import pl.spring.demo.enumerations.BookStatus;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

/**
 * Book controller
 * 
 * @author mmotowid
 *
 */
@Controller
@RequestMapping("/books")
public class BookController {

	@Autowired
	private BookService bookService;

	@ModelAttribute("newBook")
	public BookTo construct() {
		return new BookTo();
	}

	/**
	 * Lists all books from the database
	 * @param model
	 * @return model and view
	 */
	@RequestMapping
	public ModelAndView list(Model model) {
		ModelAndView modelAndView = new ModelAndView();
		List<BookTo> bookList = bookService.findAllBooks();
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookList);
		modelAndView.setViewName(ViewNames.BOOKS);
		return modelAndView;
	}
	
	/**
	 * Method shows book with a specified ID
	 * @param id
	 * @param model
	 * @return model and view
	 */

	@RequestMapping("/book")
	public ModelAndView list(@RequestParam("id") Long id, Model model) {
		ModelAndView modelAndView = new ModelAndView();
		BookTo bookById = bookService.findBookById(id);
		if (bookById == null) {
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "Invalid id number, there is no book with this id");
			modelAndView.setViewName(ViewNames._403);
		} else {
			modelAndView.addObject(ModelConstants.BOOK, bookById);
			modelAndView.setViewName(ViewNames.BOOK);
		}
		return modelAndView;
	}

	/**
	 * Method searches database via author and title. Checks if there are no
	 * duplicated objects
	 * 
	 * @param authors
	 * @param title
	 * @param model
	 * @return model and view
	 */
	@RequestMapping("/searched")
	public ModelAndView list(@RequestParam("authors") String authors, @RequestParam("title") String title,
			Model model) {
		ModelAndView modelAndView = new ModelAndView();
		List<BookTo> booksByAuthor = new ArrayList<>();
		List<BookTo> booksByTitle = new ArrayList<>();
		if (!authors.isEmpty()) {
			booksByAuthor = bookService.findBooksByAuthor(authors);
		}
		if (!title.isEmpty()) {
			booksByTitle = bookService.findBooksByTitle(title);
		}
		if (booksByAuthor.isEmpty() && booksByTitle.isEmpty()) {
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "We couldn't find any matches for your request");
			modelAndView.setViewName(ViewNames._403);
			return modelAndView;
		} else {
			List<BookTo> searchResultList = new ArrayList<>(booksByAuthor);
			booksByTitle.removeAll(booksByAuthor);
			searchResultList.addAll(booksByTitle);
			modelAndView.addObject(ModelConstants.BOOK_LIST, searchResultList);
			modelAndView.setViewName(ViewNames.SEARCHED);
			return modelAndView;
		}
	}

	/**
	 * Method shows web form to provide data for new book creation
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ModelConstants.BOOK, new BookTo());
		modelAndView.setViewName(ViewNames.ADD_BOOK);
		return modelAndView;
	}

	/**
	 * Method gets gets data from the web form, creates new Book and adds it to
	 * database
	 * 
	 * @param newBook
	 * @param model
	 * @return model and view
	 */

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addBook(@ModelAttribute("newBook") BookTo newBook, Model model) {
		String authors = newBook.getAuthors();
		String title = newBook.getTitle();
		BookStatus bookStatus = newBook.getStatus();
		ModelAndView modelAndView = new ModelAndView();
		if (authors == null || title == null || bookStatus == null) {
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "All parameters must be filled");
			modelAndView.setViewName(ViewNames._403);
			return modelAndView;
		} else {
			bookService.saveBook(newBook);
			modelAndView.addObject(ModelConstants.INFO, "New Book succesfully created");
			modelAndView.addObject(ModelConstants.BOOK, newBook);
			modelAndView.setViewName(ViewNames.BOOK_ADDED);
			return modelAndView;
		}
	}

	/**
	 * Method deletes book with specified id
	 * 
	 * @param id
	 * @return model and View
	 */

	@RequestMapping(value = "/deleteBook")
	public ModelAndView deleteBook(@RequestParam Long id) {
		ModelAndView modelAndView = new ModelAndView();
		BookTo bookById = bookService.findBookById(id);
		if (bookById == null) {
			modelAndView.addObject(ModelConstants.ERROR_MESSAGE, "Couldn't find a book with specified id");
			modelAndView.setViewName(ViewNames._403);
		} else {
			modelAndView.addObject(ModelConstants.BOOK, bookById);
			bookService.deleteBook(id);
			modelAndView.setViewName(ViewNames.BOOK_DELETED);
		}

		return modelAndView;
	}

	/**
	 * Method collects info about all books to choose for delete
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView allBooks() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		modelAndView.setViewName(ViewNames.DELETE_BOOK);
		return modelAndView;
	}

	/**
	 * Binder initialization
	 */
	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		binder.setAllowedFields("id", "title", "authors", "status");
	}

}
