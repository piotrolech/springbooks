<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Book</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>Book successfully deleted</h1>
			</div>
			<div>
				<a href="<c:url value="/" />"
					class="btn btn-danger btn-mini pull-right">home</a>
			</div>
			<div>
				<a href="<c:url value="/j_spring_security_logout" />"
					style="margin-right: 10px"
					class="btn btn-danger btn-mini pull-right">logout</a>
			</div>
		</div>
	</section>
	<section class="container">
		<div class="col">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<h3>${book.id}</h3>
				<p>
					<strong>Book title: </strong>${book.title}
				</p>
				<p>
					<strong>Wrote by</strong>: ${book.authors}
				</p>
				<p>
					<a href="<spring:url value="/books/delete" />"
						class="btn btn-lg btn-success btn-block"> Delete another Book
					</a>

				</p>

			</div>
		</div>
	</section>
</body>
</html>