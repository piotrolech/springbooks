<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>BookStore</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>${greeting}</h1>
				<p>${info}</p>
			</div>
			<div>
				<a href="<c:url value="/j_spring_security_logout" />"
					style="margin-right: 10px"
					class="btn btn-danger btn-mini pull-right">logout</a>
			</div>
		</div>
	</section>
	<section class="container">
		<div class="col">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<div class="thumbnail">
					<div class="caption">
						<h3>Books</h3>
						<p>Display all books</p>
						<p>
							<a href="/webstore/books"
								class="btn btn-lg btn-success btn-block"> Show all books </a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<div class="thumbnail">
					<div class="caption">
						<h3>Add book</h3>
						<p>Admin tool to create new book</p>
						<p>
							<a href="/webstore/books/add"
								class="btn btn-lg btn-success btn-block"> Add book </a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<div class="thumbnail">
					<div class="caption">
						<h3>Delete book</h3>
						<p>Librarian tool to delete book</p>
						<p>
							<a href="/webstore/books/delete"
								class="btn btn-lg btn-success btn-block"> Delete book </a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<div class="thumbnail">
					<div class="caption">
						<h3>Search</h3>
						<p>Search for books with given author or title</p>
					</div>
					<form action="<c:url value="/books/searched"></c:url>" method=post>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="authors" name='authors'
									type="text">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="title" name='title'
									type="text">
							</div>
							<input class="btn btn-lg btn-success btn-block" type="submit"
								value="search">
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="container"></section>
</body>
</html>
