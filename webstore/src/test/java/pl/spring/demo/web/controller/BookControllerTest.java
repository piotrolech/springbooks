package pl.spring.demo.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import pl.spring.demo.constants.ModelConstants;
import pl.spring.demo.constants.ViewNames;
import pl.spring.demo.controller.BookController;
import pl.spring.demo.enumerations.BookStatus;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "controller-test-configuration.xml")
@WebAppConfiguration
public class BookControllerTest {

	@Autowired
	private BookService bookService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		Mockito.reset(bookService);

		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");

		BookController bookController = new BookController();
		mockMvc = MockMvcBuilders.standaloneSetup(bookController).setViewResolvers(viewResolver).build();
		ReflectionTestUtils.setField(bookController, "bookService", bookService);

	}

	@Test
	public void testgetAllBooksPage() throws Exception {
		// given
		List<BookTo> testBooks = new ArrayList<>();
		BookTo testBookOne = new BookTo(1L, "Book One", "Test Author", BookStatus.FREE);
		BookTo testBookTwo = new BookTo(2L, "Book Two", "Test Author", BookStatus.FREE);
		BookTo testBookThree = new BookTo(3L, "Book Three", "Test Author", BookStatus.FREE);
		BookTo testBookFour = new BookTo(4L, "Book Four", "Test Author", BookStatus.FREE);
		testBooks.add(testBookOne);
		testBooks.add(testBookTwo);
		testBooks.add(testBookThree);
		testBooks.add(testBookFour);
		Mockito.when(bookService.findAllBooks()).thenReturn(testBooks);

		// attribute
		ResultActions resultActions = mockMvc.perform(post("/books").flashAttr(ModelConstants.BOOK_LIST, testBooks));

		// then
		resultActions.andExpect(view().name(ViewNames.BOOKS))
				.andExpect(model().attribute(ModelConstants.BOOK_LIST, new ArgumentMatcher<Object>() {
					@Override
					public boolean matches(Object argument) {
						@SuppressWarnings("unchecked")
						List<BookTo> books = (List<BookTo>) argument;
						return null != books && books.equals(testBooks);
					}
				}));
	}

	@Test
	public void testAddBook() throws Exception {
		// given
		BookTo testBook = new BookTo(1L, "Test title", "Test Author", BookStatus.FREE);
		Mockito.when(bookService.saveBook(Mockito.any())).thenReturn(testBook);

		// attribute
		ResultActions resultActions = mockMvc.perform(post("/books/add").flashAttr(ModelConstants.BOOK, testBook));

		// then
		resultActions.andExpect(model().attribute(ModelConstants.BOOK, new ArgumentMatcher<Object>() {

			@Override
			public boolean matches(Object argument) {
				BookTo book = (BookTo) argument;
				return null != book && testBook.getTitle().equals(book.getTitle());
			}
		}));
	}

	@Test
	public void testShoudlNotAddBook() throws Exception {
		// given
		BookTo testBook = new BookTo(1L, "", "Test Author", BookStatus.FREE);

		// when
		ResultActions resultActions = mockMvc
				.perform(post("/books/add").flashAttr(ModelConstants.ERROR_MESSAGE, testBook));

		// then
		resultActions.andExpect(view().name(ViewNames._403))
				.andExpect(model().attribute(ModelConstants.ERROR_MESSAGE, new ArgumentMatcher<Object>() {

					@Override
					public boolean matches(Object argument) {
						String message = (String) argument;
						return null != message && message.length() > 0;
					}
				}));
	}
}
